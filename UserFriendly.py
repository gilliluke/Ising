#!/usr/bin/env python

import numpy as np
import random as rand
import matplotlib.pyplot as plt

class cities:
	#defines class that stores the city name and position
	citycount=0

	def __init__(self,name,position):
		self.name=name
		self.position=position
		cities.citycount += 1

	def displayCount(self):
		print "Total number of cities %d" % self.citycount

	def displayCity(self):
		print "Name: ", self.name, "Position: ", self.position


#an input sequence that allows yoou to define a new object in the class by inputting the cities you want on your route

citylist=[]
x="word"
print "Input cities you want to incorporate on your route." 
N=True
while N==True:
	x=raw_input("City Name: ")
	y=input("Latitude: ")
	z=input("Longitude: ")
        city1=cities(x,[111*y,88.8*z])
	citylist.append(city1)
        city1.displayCount()
	a=raw_input("Continue? Y/N ")
	if a== "Y":
		N=True
	else:
		N=False

def dist(S,n):
	#calculates the distance to travel for a given input array
	nint=np.arange(0,n-1,1)
	distlist=[]
	for i in nint:
		
		if i>=n-1:
			dist=0
		else:
			dist=np.sqrt(((S[i+1][0]-S[i][0])**2)+((S[i+1][1]-S[i][1])**2))
		distlist.append(dist)
	total=np.sum(distlist)	
	return total

def ann(T,it):
	iterations=0
	S=citylist
	n=cities.citycount
	
	while iterations<=it:
		#appends the positions into a list from the class to run it through the distance function
		d1list=[]
		b=0
		while b<(n):
			w=citylist[b].position
			d1list.append(w)
			
			b=b+1
		d1=dist(d1list,n)
		x=rand.randint(0,n-1)
		y=rand.randint(0,n-1)
		#swaps the positions of two random cities
		citylist[x],citylist[y]=citylist[y],citylist[x]
		d2list=[]
		b=0
		while b<(n):
			w=citylist[b].position
			d2list.append(w)
			b=b+1
		d2=dist(d2list,n)
		print d2
		#comparison to see if change is accepted or rejected
		if d2<d1:
			citylist[x],citylist[y]=citylist[x],citylist[y]
		else:
			if rand.random()<= np.exp(-((d2-d1)/T)):
				citylist[x],citylist[y]=citylist[x],citylist[y]
			else:
				citylist[x],citylist[y]=citylist[y],citylist[x]
		
		iterations=iterations+1
		plt.plot(iterations, d2, 'b.')
	#outputs the names of the cities in the optimised order
	names=[]
	b=0
	while b<(n):
		w=citylist[b].name
		names.append(w)
		b=b+1
	print names
	plt.show()

	return citylist
	
ann(0.01,100)
	






