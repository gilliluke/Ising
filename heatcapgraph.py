#!/usr/bin/env python

import Ising as Is
import matplotlib.pyplot as plt
import numpy as np

y=np.arange(4,0.1,-0.1)
n=32
h=0
for T in y:
	
	S=Is.Ising(n,T,50,h)
	E=Is.energies(S,n,h)

	heatcap=Is.heatcap(E,n,T)

	plt.plot(T,heatcap,'b.')



plt.show()
