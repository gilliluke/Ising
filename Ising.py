#!/usr/bin/env python

import init
import numpy as np
import matplotlib.pyplot as plt

def neighbours(S,n,i,j):
	l=(i-1,j)
	r=(i+1 if i+1<=n else 0,j)
	u=(i,j-1)
	d=(i,j+1 if j+1<=n else 0)
	return [S[l[0],l[1]],
		S[r[0],r[1]],
		S[u[0],u[1]],
		S[d[0],d[1]]]

def Ising(n,T,it,h):
	
	iterations=0
	J=1
	k_B=1
	S=init.init(n)
	#S=np.ones((n,n))
	
	
	while iterations <= it:
	
		nind=np.arange(-1,n-1,1)
		for i in nind:
			for j in nind:
				delta_H=((-2)*S[i][j])*(J*np.sum(neighbours(S,n,i,j))-h)
		
		
				if delta_H>=0:
					S[i][j]=-S[i][j]
		
				else:
					if np.random.random()<=np.exp((delta_H)/(k_B*T)):
						S[i][j]=-S[i][j]
					else:
						S[i][j]=S[i][j]
					
		iterations=iterations+1
	return S

def mag(S,n):
	y=np.abs(np.sum(S))*(n**(-2))
	print y
	return y


def magsus(S,n,T):
	y=(1/T)*(((np.abs(np.sum(S**2)))*(n**(-2)))-((np.abs(np.sum(S))*(n**(-2)))**2))
	print y
	return y

def energies(S,n,h):
	
	J=1
	k_B=1
	
	delta_Hmatrix=np.zeros((n,n))
	
	nind=np.arange(-1,n-1,1)
	for i in nind:
		for j in nind:
			delta_H=((-2)*S[i][j])*(J*np.sum(neighbours(S,n,i,j))-h)
			delta_Hmatrix[i][j]=delta_H
	return delta_Hmatrix

def heatcap(S,n,T):
	k_B=1
	y=(1/(k_B*(T**2)))*(((np.abs(np.sum(S**2)))*(n**(-2)))-((np.abs(np.sum(S))*(n**(-2)))**2))
	print y
	return y

