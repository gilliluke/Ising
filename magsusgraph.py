#!/usr/bin/env python

import Ising as Is
import matplotlib.pyplot as plt
import numpy as np

y=np.arange(5,0.1,-0.1)
n=32
h=0
for T in y:
	S=Is.Ising(n,T,1000,h)

	magsus=Is.magsus(S,n,T)

	plt.plot(T,magsus,'b.')



plt.show()
