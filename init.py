#!/usr/bin/env python

import numpy as np
import random as rand


def init(n):

	spinstates=np.array([-1,1])

	spin_matrix = np.random.choice(spinstates, (n,n))
	
	return spin_matrix
