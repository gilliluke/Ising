#!/usr/bin/env python

import numpy as np
import random as rand
import matplotlib.pyplot as plt

def init(n):
	#initialises a randomised list of coordinate points (cities)
	iterations=0
	city=[]
	while iterations<=n:
		y=(rand.randint(1,1001),rand.randint(1,1001))
		city.append(y)
		iterations = iterations + 1
	return city

def dist(S,n):
	#calculates distance necessary to travel to go to each point on the list in the order they're in
	nint=np.arange(0,n-1,1)
	distlist=[]
	for i in nint:
		
		if i>=n-1:
			dist=0
		else:
			dist=np.sqrt(((S[i+1][0]-S[i][0])**2)+((S[i+1][1]-S[i][1])**2))
		distlist.append(dist)
	total=np.sum(distlist)	
	return total

def Temp(T,it):
	#updates the 'temperature' to simulate annealing
	y=T/np.log(it+1)
	return y

def ann(n,it):
	iterations=1
	S=init(n)
	nint=np.arange(0,n,1)
	finale=[]
	T=10000
	
	while iterations<=(it+1):
		
		#calculates initial distance to travel
		d1=dist(S,n)
		x=rand.randint(0,n-1)
		y=rand.randint(0,n-1)
		#swaps two random positions in the list
		S[x],S[y]=S[y],S[x]
		#calculates distance after the swap
		d2=dist(S,n)
		print d2
		#deciding whether to accept or reject the change
		if d2<d1:
			S[x],S[y]=S[x],S[y]
		else:
			if rand.random()<= np.exp(-((d2-d1)/T)):
				S[x],S[y]=S[x],S[y]
			else:
				S[x],S[y]=S[y],S[x]
		

		plt.plot(iterations, d2, 'b.')
		finale.append(d2)
		T=Temp(T,iterations)
        	iterations=iterations+1

	plt.title("Travelling Salesman Implementation for %d Cities on Route" % n)
	plt.xlabel("Iterations")
	plt.ylabel("Total Distance To Travel on Route (km)")
	plt.xlim(0,it)
	plt.ylim(min(finale),max(finale))
	
	print "The initial distance to travel was %d km." % max(finale)
	print "After implementing the code the distance to travel was %d km." % min(finale)
	plt.show()
	return S

ann(10,10000)
