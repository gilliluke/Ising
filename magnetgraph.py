#!/usr/bin/env python

import Ising as Is
import matplotlib.pyplot as plt
import numpy as np

y=np.arange(0.01,5,0.01)
n=10
h=0
for T in y:
	S=Is.Ising(n,T,500,h)

	magnetisation=Is.mag(S,n)
	plt.plot(T,magnetisation,'b.')


plt.title("Magnetisation of Lattice vs. Temperature")
plt.xlabel("Temperature $(JK^{-1})$")
plt.ylabel("Magetisation")
plt.show()
